# desafio

> Desafio con la tecnologia vuejs, vuex , vue-router , vuelidate .
> Consideraciones:
> Saludos, practicamente no he tenido mucho tiempo para desarrollar todo el desafio,
> el lunes se me notifico que aun podia hacerlo y basicamente comenze el martes.
> dado este caso apenas tuve 3 dias para poderlo hacer por ende no se cumplieron
> todos los requerimientos del desafio.
> Seguidamente enlistare los requerimientos y especificaciones de lo que esta hecho.
> Primeramente :
> Por el momento solo se cuenta con un requerimiento el cual es el tabulado filtrado y vista del recurso de peliculas.
> Aun esta Pendiente la paginación.
> Y el filtrado por fechas puesto que no entiendo como funcionan estas fechas ya que nunca he visto starwars
> y ni idea de como funciona esto jaja, por esto omiti este filtro.
> Esta pendiente el tabulado filtrado y vista de todo lo relacionado con peliculas
> ya que por el poco tiempo que tuve no dio lugar a programar esto.
> sin embargo es bastante parecido al primer requerimiento salvo consideraciones.
> Los componentes de esto esta creado y se puede apreciar el funcionamiento seleccionando peliculas o personajes en el combobox inicial seguidamente de pulsar el boton search.
> Con respecto al diseño, vuelvo y repito que por el poco tiempo no dio lugar a un diseño mas agradable, de hecho hay cosas que aun ni diseño tienen, se opto por un diseño muy basico puramente para la apreciacion del funcionamiento del aplicativo.
> Y por ultimo el primer requerimiento, no entendi que papel jugaba el cuadro de texto solo para los personajes
> por esa razon no aparece.
> Solamente y cualquier duda contactarme a mi correo <devfernando95@gmail.com> o igual desde el postulamiento
> en Getonboard.

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
