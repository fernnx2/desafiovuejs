import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    personajes: [],
    peliculas: [],
    species: [],
    homeworld: [],
    allHomeworld: [],
    showPersonaje: null,
    showPelicula: null,
    detallePersonaje: [],
    index: null
  }
});
